﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private static List<Message> messages = new List<Message>();
        private static int counter = 0;
        static MessagesController()
        {
            messages.Add(new Message { Id = 1, Sender = "Danny", Body = "Hello from Danny" });
            messages.Add(new Message { Id = 2, Sender = "Galit", Body = "How are you Danny?" });
            messages.Add(new Message { Id = 3, Sender = "Danny", Body = "I'm good." });
            messages.Add(new Message { Id = 4, Sender = "Steve", Body = "What's up?" });
            counter = messages.Count;
        }

        // GET api/values
        [HttpGet]
        public List<Message> Get()
        {
            return messages;
        }

        // GET api/values/5
        [HttpGet("searchfilter")]
        public List<Message> Get(int id = 0, string sender = null, string body = null)
        {
            //Message result = messages.FirstOrDefault(m => m.Id == id);
            if (id != 0)
            {
                var result1 = messages.FirstOrDefault(_ => _.Id == id);
                return result1 == null ? new List<Message>() : new List<Message>(new[] { result1});
            }

            List<Message> result = new List<Message>();
            if (sender != null)
            {
                result.AddRange(messages.Where(_ => _.Sender.ToUpper().Contains(sender.ToUpper())).ToList());
            }
            if (body!= null)
            {
                result.AddRange(messages.Where(_ => _.Body.ToUpper().Contains(body.ToUpper())).ToList());
            }
            return result;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Message Get(int id)
        {
            Message result = messages.FirstOrDefault(m => m.Id == id);
            return result;
        }

        [HttpPost]
        // POST api/values
        public void Post([FromBody] Message message)
        {
            message.Id = ++counter;
            messages.Add(message);
        }

        [HttpPut("{id}")]
        // PUT api/values/5
        public void Put(int id, [FromBody] Message message)
        {
            Message result = messages.FirstOrDefault(m => m.Id == id);
            if (result != null)
            {
                result.Sender = message.Sender;
                result.Body = message.Body;
            }
        }

        [HttpDelete("{id}")]
        // DELETE api/values/5
        public void Delete(int id)
        {
            Message result = messages.FirstOrDefault(m => m.Id == id);
            if (result != null)
                messages.Remove(result);

        }

    }
}
