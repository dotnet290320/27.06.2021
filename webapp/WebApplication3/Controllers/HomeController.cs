﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AcceptPageGET()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><body>");
            foreach (var key in Request.Query.Keys)
            {
                sb.Append($"<h1>{key} : {Request.Query[key]} </h1>");
            }
            sb.Append("</html>");
            return Content(sb.ToString());

        }

        //[HttpPost]
        public ActionResult AcceptPagePOST()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><body>");
            foreach (var key in Request.Form.Keys)
            {
                sb.Append($"<h1>{key} : {Request.Form[key]} </h1>");
            }
            sb.Append("</html>");
            //return Content(sb.ToString());

            if (Request.Form["pwd"] != "1234")
            {
                return View("wrong");
            }
            else
            {
                return View("app");
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
